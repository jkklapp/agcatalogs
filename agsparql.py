'''
agsparql.py - wrapper to access aginfra sparql endpoints.

Jaakko Lappalainen 2013, jkk.lapp@gmail.com
'''
from SPARQLWrapper import SPARQLWrapper, JSON

class Catalog:
	def MakeSparqlQuery(self,sparql,query):
		sparql.setQuery(query)
		sparql.setReturnFormat(JSON)
		results=sparql.query().convert()
		return results
	def ParseResults(self,results,l=None):
		v = []
		for r in results["results"]["bindings"]:
			for x in r.keys() if l is None else l:
				v.append(r[x]["value"])
		return v
	def ListDatasetTypes(self):
		sparql = SPARQLWrapper("http://4store.ipb.ac.rs:81/sparql/")
		queryString = "select distinct ?d where {?x <http://aginfra.ipb.ac.rs/term/DatasetType> ?d }"
		results = self.MakeSparqlQuery(sparql,queryString)
		return self.ParseResults(results,['d'])
	def ListMetaSchemes(self):
		sparql = SPARQLWrapper("http://4store.ipb.ac.rs:81/sparql/")
		queryString = "select distinct ?d where {?x <http://aginfra.ipb.ac.rs/term/metaScheme> ?d }"
		results = self.MakeSparqlQuery(sparql,queryString)
		return self.ParseResults(results,['d'])
	def ListInfo(self,info):
		sparql = SPARQLWrapper("http://4store.ipb.ac.rs:81/sparql/")
		queryString = "select distinct ?d where {?x "+info+" ?d }"
		results = self.MakeSparqlQuery(sparql,queryString)
		return self.ParseResults(results,['d'])
