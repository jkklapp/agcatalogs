'''
example.py - sample client

Jaakko Lappalainen 2013, jkk.lapp@gmail.com
'''
from agcatalogs import DatasetCatalog,ServiceCatalog
import sys

print "Selecting text-based datasets..."
agdc = DatasetCatalog()
print agdc.ListDatasetTypes()
results = agdc.ListDatasetsByType('TextBased')
print "Selecting one dataset..."
d = results[0]
print d
#raw_input("")
print "Info about this dataset"
print agdc.GetDatasetAccessURL(d)
print "Lets find out ag:metaScheme for this dataset..."
#raw_input("")
print agdc.GetDatasetMetaScheme(d)
print "Now lets see what ag:TextBased services are discoverable..."
agsc = ServiceCatalog()
print agsc.ListDatasetTypes()
results = agsc.ListServicesByInputDatasetType('TextBased')
print "Lets find out more about the translator..."
s = results[2]
print s
#raw_input("")
agsc.GetServiceInputDatasetType(s)
#raw_input("")
print "Now we can invoke the service using EntryPoint and the dataset..."
