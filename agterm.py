'''
services.py - wrapper to access aginfra terminology.

Jaakko Lappalainen 2013, jkk.lapp@gmail.com
'''

class TermNotSupportedError(Exception):
	def __init__(self,t):
		self.type = t
	def __str__(self):
		return "TermNotSupportedError: DatasetType '"+self.type+"' not supported."
class AgTerm:
	names = {}
	names['ag']="http://aginfra.ipb.ac.rs/term/"
	names['dcat']="http://vocab.deri.ie/dcat#"
	names['dct']="http://purl.org/dc/terms/"
	names['foaf']="http://xmlns.com/foaf/0.1/"
	def Urify(self,ns,t):
		if t.find("<http://") != -1:
			return t
		elif t.find("http://") != -1:
			return "<"+t+">"
		else:
			return "<"+self.names[ns]+t+">"	