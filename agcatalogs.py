'''
	datasets.py - wrapper to access the dataset catalog and perform some basic dataset 
	handling.

	Jaakko Lappalainen 2013, jkk.lapp@gmail.com
'''
from agterm import AgTerm,TermNotSupportedError
from agsparql import Catalog
from SPARQLWrapper import SPARQLWrapper, JSON


class DatasetCatalog(Catalog,AgTerm):
	def __init__(self):
		self.sparql = SPARQLWrapper("http://4store.ipb.ac.rs:81/sparql/")
		self.names=AgTerm.names
	def ListDatasets(self,pred, obj, offset=None, limit=None):
		offset_str=""
		if offset is not None:
			offset_str=" offset "+offset 
		limit_str="" 
		if limit is not None:
			limit_str=" limit "+limit
		queryString = "select distinct ?d where {?d "+self.Urify('ag',pred)+" "+self.Urify('ag',obj)+" }"+offset_str+limit_str
		return self.MakeSparqlQuery(self.sparql,queryString)
	def ListDatasetsByType(self,t, offset=None, limit=None):
		if self.Urify('ag',t).replace('<','').replace('>','') not in self.ListDatasetTypes():
			raise TermNotSupportedError(t)
		results = self.ListDatasets("DatasetType", t, offset, limit)
		return self.ParseResults(results)
	def ListDatasetsByMetaScheme(ms, offset=None, limit=None):
		if self.Urify('ag',ms).replace('<','').replace('>','') not in term.ListMetaSchemes():
			raise TermNotSupportedError(ms)
		return ListDatasets("metaScheme", ms, offset, limit)
	def GetRawDatasetInfo(self,id):
		queryString = "select * where {<"+id+"> ?a ?b }"
		return self.MakeSparqlQuery(self.sparql,queryString)
	def GetDatasetInfo(self,id,info):
		queryString = "select distinct ?info where {<"+id+"> "+info+" ?info }"
		results = self.MakeSparqlQuery(self.sparql,queryString)
		return self.ParseResults(results,['info'])
	def GetDatasetMetaScheme(self,id):
		return self.GetDatasetInfo(id,self.Urify('ag','metaScheme'))
	def GetDatasetDatasetType(self,id):
		return self.GetDatasetInfo(id,self.Urify('ag','DatasetType'))
	def GetDatasetAccessURL(self,id):
		return self.GetDatasetInfo(id,self.Urify('dcat','accessURL'))

class ServiceCatalog(Catalog, AgTerm):
	def __init__(self):
		self.sparql = SPARQLWrapper("http://4store.ipb.ac.rs:82/sparql/")
		self.names=AgTerm.names
	def ListServices(self, pred, obj, offset=None, limit=None):
		offset_str=""
		if offset is not None:
			offset_str=" offset "+offset 
		limit_str="" 
		if limit is not None:
			limit_str=" limit "+limit
		queryString = "select distinct ?d where {?d "+self.Urify('ag',pred)+" "+self.Urify('ag',obj)+" }"+offset_str+limit_str
		return self.MakeSparqlQuery(self.sparql,queryString)
	def ListServicesByInputDatasetType(self,t, offset=None, limit=None):
		if self.Urify('ag',t).replace('<','').replace('>','') not in self.ListDatasetTypes():
			raise TermNotSupportedError(t)
		results = self.ListServices("InputDatasetType", t, offset, limit)
		return self.ParseResults(results,['d'])	
	def ListServicesByOutputDatasetType(self,t, offset=None, limit=None):
		if self.Urify('ag',t).replace("<","").replace(">","") not in self.ListDatasetTypes():
			raise TermNotSupportedError(t)
		results = self.ListServices("OutputDatasetType", t, offset, limit)
		return self.ParseResults(results,['d'])
	def GetServiceInfo(self,id,info):
		queryString = "select distinct ?info where {<"+id+"> "+info+" ?info }"
		results = self.MakeSparqlQuery(self.sparql,queryString)
		return self.ParseResults(results,['info'])
	def GetServiceInputDatasetType(self,id):
		return self.GetServiceInfo(id,self.Urify('ag','InputDatasetType'))
	def GetServiceOutputDatasetType(self,id):
		return self.GetServiceInfo(id,self.Urify('ag','OutputDatasetType'))
	def GetServiceEntryPoint(self,id):
		return self.GetServiceInfo(id,self.Urify('ag','EntryPoint'))
